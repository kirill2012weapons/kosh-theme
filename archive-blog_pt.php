<?php get_header(); ?>

<?php
$archiveBlogArgs = [
  'post_type' => 'blog_pt',
  'post_status' => 'publish',
  'posts_per_page' => -1,
];
$blogArchive = new WP_Query($archiveBlogArgs);
?>

<div class="ps-blog-grid pt-80 pb-80">
  <div class="ps-container">
    <div class="row">
      <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 ">

        <?php if (!empty($blogArchive->posts)) : ?>

          <?php foreach ($blogArchive->posts as $new) : ?>
            <?php
            /**
             * @var $new WP_Post
             * @var $author WP_User
             */
            $id = $new->ID;
            $author = get_user_by('id', $new->post_author);
            ?>

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
              <div class="ps-post mb-30">
                <div class="ps-post__thumbnail">
                  <a class="ps-post__overlay" href="<?php echo get_post_permalink( $id ); ?>"></a>
                  <img src="<?php echo get_the_post_thumbnail_url($id, 'custom_size_958_401'); ?>" alt=""></div>
                <div class="ps-post__content">
                  <a class="ps-post__title" href="<?php echo get_post_permalink( $id ); ?>"><?php echo get_the_title( $id ); ?></a>
                  <p class="ps-post__meta">
                    <span>Опубликовано:<span class="mr-5"><?php echo $author->nickname; ?></span>
                    </span> -<span class="ml-5"><?php echo get_the_date('j F Y', $id); ?></span>
                  </p>
                  <p><?php echo wp_trim_words(wp_strip_all_tags( get_the_content('', false, $id) ), 10, '...'); ?></p>
                  <a class="ps-morelink" href="<?php echo get_post_permalink( $id ); ?>">
                    Читать далее<i
                        class="fa fa-long-arrow-right"></i></a>
                </div>
              </div>
            </div>

          <?php endforeach; ?>

        <?php else: ?>
          Новостей пока еще нет.
        <?php endif; ?>

        <?php if (false) : ?>

          <div class="mt-30">
            <div class="ps-pagination">
              <ul class="pagination">
                <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">...</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
              </ul>
            </div>
          </div>

        <?php endif; ?>
      </div>

      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
        <?php echo render('aside/blog.php'); ?>
      </div>

    </div>
  </div>
</div>

<?php get_footer(); ?>
