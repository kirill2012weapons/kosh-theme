<?php

/******************************************************************
 *
 * ░░░░░░░░░░░░░░░░░░░░░▄▀░░▌
 * ░░░░░░░░░░░░░░░░░░░▄▀▐░░░▌
 * ░░░░░░░░░░░░░░░░▄▀▀▒▐▒░░░▌
 * ░░░░░▄▀▀▄░░░▄▄▀▀▒▒▒▒▌▒▒░░▌
 * ░░░░▐▒░░░▀▄▀▒▒▒▒▒▒▒▒▒▒▒▒▒█
 * ░░░░▌▒░░░░▒▀▄▒▒▒▒▒▒▒▒▒▒▒▒▒▀▄
 * ░░░░▐▒░░░░░▒▒▒▒▒▒▒▒▒▌▒▐▒▒▒▒▒▀▄
 * ░░░░▌▀▄░░▒▒▒▒▒▒▒▒▐▒▒▒▌▒▌▒▄▄▒▒▐
 * ░░░▌▌▒▒▀▒▒▒▒▒▒▒▒▒▒▐▒▒▒▒▒█▄█▌▒▒▌
 * ░▄▀▒▐▒▒▒▒▒▒▒▒▒▒▒▄▀█▌▒▒▒▒▒▀▀▒▒▐░░░▄
 * ▀▒▒▒▒▌▒▒▒▒▒▒▒▄▒▐███▌▄▒▒▒▒▒▒▒▄▀▀▀▀
 * ▒▒▒▒▒▐▒▒▒▒▒▄▀▒▒▒▀▀▀▒▒▒▒▄█▀░░▒▌▀▀▄▄
 * ▒▒▒▒▒▒█▒▄▄▀▒▒▒▒▒▒▒▒▒▒▒░░▐▒▀▄▀▄░░░░▀
 * ▒▒▒▒▒▒▒█▒▒▒▒▒▒▒▒▒▄▒▒▒▒▄▀▒▒▒▌░░▀▄
 * ▒▒▒▒▒▒▒▒▀▄▒▒▒▒▒▒▒▒▀▀▀▀▒▒▒▄▀
 *
 * Написал про3кт - @kirill2012weapons@gmail.com
 * Целую В щечку
 * Того кто будет читать мой код - Люблю тебя :)
 *
 ******************************************************************/

add_theme_support( 'woocommerce' );

function logCustom($message) {
  error_log(print_r($message, true));
}

/******************************************************************
 *
 * ▄██████████████▄▐█▄▄▄▄█▌
 * ██████▌▄▌▄▐▐▌███▌▀▀██▀▀
 * ████▄█▌▄▌▄▐▐▌▀███▄▄█▌
 * ▄▄▄▄▄██████████████▀
 *
 * НЕНАВИЖУ WP
 *
 ******************************************************************/
require_once ABSPATH . 'wp-content/themes/Kosh/src/parts_functions/define_global_vars.php';
require_once ABSPATH . 'wp-content/themes/Kosh/src/parts_functions/template_part_custom.php';
require_once ABSPATH . 'wp-content/themes/Kosh/src/parts_functions/enq_scripts.php';
require_once ABSPATH . 'wp-content/themes/Kosh/src/parts_functions/custom_upload.php';
require_once ABSPATH . 'wp-content/themes/Kosh/src/parts_functions/woo_mailer/mailer.php';
require_once ABSPATH . 'wp-content/themes/Kosh/src/parts_functions/woo_ajax.php';
require_once ABSPATH . 'wp-content/themes/Kosh/src/parts_functions/authenticate.php';
require_once ABSPATH . 'wp-content/themes/Kosh/src/parts_functions/admin_init_options.php';
require_once ABSPATH . 'wp-content/themes/Kosh/src/parts_functions/custom_pagination.php';

/**
 * Redirect if just subscriber
 */
add_action('admin_init', 'wpse66093_no_admin_access', 100);
function wpse66093_no_admin_access() {
  if (is_ajax()) return;

  $redirect = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER']:home_url('/account/');
  if (
    current_user_can('subscriber') ||
    current_user_can('customer')
  )
    exit(wp_redirect($redirect));
}


function sv_wc_cogs_add_order_profit_column_header($columns) {
  $new_columns = array();
  foreach ($columns as $column_name => $column_info) {
    $new_columns[$column_name] = $column_info;
    if ('order_total' === $column_name) {
      $new_columns['order_shipment'] = 'ДОСТАВКА';
    }
    if ('order_total' === $column_name) {
      $new_columns['order_call'] = 'Созвон';
    }
  }
  return $new_columns;
}

add_filter('manage_edit-shop_order_columns', 'sv_wc_cogs_add_order_profit_column_header', 20);

function sv_wc_cogs_add_order_profit_column_content($column) {
  global $post;
  if ('order_shipment' == $column) {
    $customShipment = get_field('o_g_status_shipments', $post->ID);

    if (is_array($customShipment)) {
      if (is_null($customShipment)) echo '<mark class="order-status status-completed tips" style="    background: #ffd91a;"><span>Не отправлен</span></mark>';
      else if ($customShipment['value'] == 'shipment_wait') echo '<mark class="order-status status-completed tips" style="    background: #ffd91a;"><span>Не отправлен</span></mark>';
      else if ($customShipment['value'] == 'shipment_send') echo '<mark class="order-status status-completed tips" style="background: #1ac9ff;"><span>' . $customShipment['label'] . '</span></mark>';
    } else {
      if (is_null($customShipment)) echo '<mark class="order-status status-completed tips" style="background: #ffd91a;"><span>Не отправлен</span></mark>';
      else if ($customShipment == 'shipment_wait') echo '<mark class="order-status status-completed tips" style="background: #ffd91a;"><span>Не отправлен</span></mark>';
      else if ($customShipment == 'shipment_send') echo '<mark class="order-status status-completed tips" style="background: #1ac9ff;"><span>ОТПРАВЛЕН</span></mark>';
    }

  }
  if ('order_call' == $column) {
    $customShipment = get_field('o_g_call', $post->ID);

    if (is_array($customShipment)) {
      if (is_null($customShipment)) echo '<mark class="order-status status-completed tips" style="    background: #ff0700;"><span>НЕТ</span></mark>';
      else if ($customShipment['value'] == 'call_wait') echo '<mark class="order-status status-completed tips" style="    background: #ff0700;"><span>НЕТ</span></mark>';
      else if ($customShipment['value'] == 'call_send') echo '<mark class="order-status status-completed tips" style="background: #00ffd2;"><span>' . 'ДА' . '</span></mark>';
    } else {
      if (is_null($customShipment)) echo '<mark class="order-status status-completed tips" style="    background: #ff0700;"><span>НЕТ</span></mark>';
      else if ($customShipment == 'call_wait') echo '<mark class="order-status status-completed tips" style="    background: #ff0700;"><span>НЕТ</span></mark>';
      else if ($customShipment == 'call_send') echo '<mark class="order-status status-completed tips" style="background: #00ffd2;"><span>' . 'ДА' . '</span></mark>';
    }

  }
}

add_action('manage_shop_order_posts_custom_column', 'sv_wc_cogs_add_order_profit_column_content');

add_image_size('custom_size_800_800', 800, 800, true);
add_image_size('custom_size_440_440', 440, 440);
add_image_size('custom_size_100_100', 100, 100);
add_image_size('custom_size_250_250', 250, 250);
add_image_size('custom_size_1920_1275', 1920, 1275);
add_image_size('custom_size_476_428', 476, 428);
add_image_size('custom_size_785_250', 785, 250);
add_image_size('custom_size_958_401', 958, 401);
add_image_size('custom_size_1918_997', 1918, 997);

add_image_size( 'test', 475, 310, array( 'center', 'center' ) );

add_image_size('custom_size_800_800_centered', 800, 800, array( 'center', 'center' ));
add_image_size('custom_size_440_440_centered', 440, 440, array( 'center', 'center' ));
add_image_size('custom_size_100_100_centered', 100, 100, array( 'center', 'center' ));
add_image_size('custom_size_250_250_centered', 250, 250, array( 'center', 'center' ));
add_image_size('custom_size_1920_1275_centered', 1920, 1275, array( 'center', 'center' ));
add_image_size('custom_size_476_428_centered', 476, 428, array( 'center', 'center' ));
add_image_size('custom_size_785_250_centered', 785, 250, array( 'center', 'center' ));
add_image_size('custom_size_958_401_centered', 958, 401, array( 'center', 'center' ));
add_image_size('custom_size_1918_997_centered', 1918, 997, array( 'center', 'center' ));


/**
 * DISABLE STANDARTS EMAILS
 */
add_filter('woocommerce_email_enabled_new_order', 'dcwd_conditionally_send_wc_email', 10, 2);
add_filter('woocommerce_email_enabled_cancelled_order', 'dcwd_conditionally_send_wc_email', 10, 2);
add_filter('woocommerce_email_enabled_customer_completed_order', 'dcwd_conditionally_send_wc_email', 10, 2);
add_filter('woocommerce_email_enabled_customer_invoice', 'dcwd_conditionally_send_wc_email', 10, 2);
add_filter('woocommerce_email_enabled_customer_note', 'dcwd_conditionally_send_wc_email', 10, 2);
add_filter('woocommerce_email_enabled_customer_on_hold_order', 'dcwd_conditionally_send_wc_email', 10, 2);
add_filter('woocommerce_email_enabled_customer_processing_order', 'dcwd_conditionally_send_wc_email', 10, 2);
add_filter('woocommerce_email_enabled_customer_refunded_order', 'dcwd_conditionally_send_wc_email', 10, 2);
add_filter('woocommerce_email_enabled_failed_order', 'dcwd_conditionally_send_wc_email', 10, 2);
function dcwd_conditionally_send_wc_email($whether_enabled, $object) {
  return false;
}

/**
 * ADD ACF OPTION PAGE
 */

if (function_exists('acf_add_options_page')) {

  acf_add_options_page(array(
    'page_title' => 'Сайт',
    'menu_title' => 'Настройки сайта',
    'menu_slug' => 'opt_st',
    'capability' => 'edit_posts',
    'redirect' => false,
    'position' => 3,
    'post_id' => 'opt_st_id',
    'update_button' => __('Сохранить', 'acf'),
    'updated_message' => __("Поздравляю паскуда. Настройки сохранены", 'acf'),
  ));

}
