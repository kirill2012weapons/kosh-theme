<?php

namespace KoshTheme\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
* @Annotation
*/
class ContainsUserNickNameExists extends Constraint
{
    public $message = 'Nickname is Exists';
}