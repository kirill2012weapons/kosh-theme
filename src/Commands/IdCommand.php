<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;

class IdCommand extends UserCommand
{
  protected $name = 'id';
  protected $description = 'Get ID';
  protected $usage = '/id';
  protected $version = '1.0.0';

  public function execute()
  {
    $message = $this->getMessage();

    $chat_id = $message->getChat()->getId();

    $data = [
      'chat_id' => $chat_id,
      'text'    =>
        'Твой код - ' . $chat_id . ' - Отдай Кириллу, закрепи чат и не выходи отсюда. ' .
        PHP_EOL .
        PHP_EOL .
        ' Номерок Кирюхи - 0995529239 ( На всякий <3 ).',
    ];

    return Request::sendMessage($data);
  }
}