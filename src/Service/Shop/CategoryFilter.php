<?php

namespace KoshTheme\Service\Shop;

use KoshTheme\Service\Shop\Engine\FilterComponent;
use KoshTheme\Service\Shop\Engine\UrlFilterGenerator;

class CategoryFilter extends FilterComponent {

  public function getRender()
  {

    $allCategoriesProduct = $this->getAllCategoriesFromProduct();

    echo render('service/shop/components/category-filter.php', [
      'categories' => $allCategoriesProduct,
    ]);

  }

  public function filterSlag()
  {
    return 'ct';
  }

  private function getArgsFromUrl()
  {

  }

  private function getAllCategoriesFromProduct()
  {

    $categories = get_categories([
      'taxonomy' => 'product_cat',
      'hide_empty' => false,
    ]);

    return $categories;

  }

}