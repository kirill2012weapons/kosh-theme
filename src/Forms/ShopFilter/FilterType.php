<?php

namespace KoshTheme\Forms\ShopFilter;


use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;

class FilterType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    /**
     * @var $cat \WP_Term
     */

    $allCategoriesProduct = get_categories([
      'taxonomy' => 'product_cat',
      'hide_empty' => false,
    ]);


    $choices = [];
    if (!empty($allCategoriesProduct)) {
      foreach ($allCategoriesProduct as $cat) {
        if ($cat->slug == 'uncategorized') continue;
        $choices[$cat->name . '(' . $cat->count . ')'] = $cat->slug;
      }
    }

    $colors = [];
    $colorsTerms = get_categories([
      'taxonomy' => 'color_tax',
      'hide_empty' => false,
    ]);
    if (!empty($colorsTerms)) {
      foreach ($colorsTerms as $cat) {
        if ($cat->slug == 'uncategorized') continue;
        $colors[$cat->name] = $cat->slug;
      }
    }


    $tints = [];
    $tintsTerms = get_categories([
      'taxonomy' => 'tint_tax',
      'hide_empty' => false,
    ]);
    if (!empty($tintsTerms)) {
      foreach ($tintsTerms as $cat) {
        if ($cat->slug == 'uncategorized') continue;
        $tints[$cat->name] = $cat->slug;
      }
    }

    $sortedBy = [
      'Дате (Новые)' => 'date_desc',
      'Дате (Старые)' => 'date_asc',
      'Цене (От дешевых к дорогим)' => 'price_asc',
      'Цене (От дорогих к дешевым)' => 'price_desc',
    ];

    $builder
      ->setMethod('GET')
      ->add('categories', ChoiceType::class, [
        'choices' => $choices,
        'multiple' => true,
        'choice_attr' => function($choice, $key, $value) {
          $allCategoriesProduct = get_categories([
            'taxonomy' => 'product_cat',
            'hide_empty' => false,
          ]);
          foreach ($allCategoriesProduct as $cat) {
            if ($cat->slug == $value && $cat->category_parent != 0) return ['class' => 'parent_isset'];
            else $parr = '';
          }
          return ['class' => $parr];
        },
      ])
      ->add('color', ChoiceType::class, [
        'choices' => $colors,
        'multiple' => true,
        'choice_attr' => function($choice, $key, $value) {
          $colorsTerms = get_categories([
            'taxonomy' => 'color_tax',
            'hide_empty' => false,
          ]);
          foreach ($colorsTerms as $cat) {
            if ($cat->slug == $value) return ['style' => 'background-color: ' . get_field('color_of_prod', $cat) . ';'];
            else $parr = '';
          }
          return ['style' => $parr];
        },
      ])
      ->add('priceMin', TextType::class, [
      ])
      ->add('priceMax', TextType::class, [
      ])
      ->add('tint', ChoiceType::class, [
        'choices' => $tints,
      ])
      ->add('sortedBy', ChoiceType::class, [
        'choices' => $sortedBy,
      ])
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults([
      'data_class' => FilterModel::class,
    ]);
  }

  public function getBlockPrefix()
  {
    return 'fltr';
  }

}