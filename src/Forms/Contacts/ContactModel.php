<?php

namespace KoshTheme\Forms\Contacts;


use KoshTheme\Validator\Constraints\ContainsUserEmailIsExists;
use Symfony\Component\Validator\Constraints as Assert;
use KoshTheme\Validator\Constraints as AcmeAssert;

class ContactModel
{

  /**
   * @Assert\NotBlank(
   *   message="Обязательное поле."
   * )
   */
  private $name;

  /**
   * @Assert\Email(
   *   message="Поле должно содержать имейл адресс."
   * )
   */
  private $email;

  /**
   * @Assert\NotBlank(
   *   message="Обязательное поле."
   * )
   */
  private $phone;

  /**
   * @Assert\NotBlank(
   *   message="Обязательное поле."
   * )
   */
  private $description;

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * @param mixed $email
   */
  public function setEmail($email) {
    $this->email = $email;
  }

  /**
   * @return mixed
   */
  public function getPhone() {
    return $this->phone;
  }

  /**
   * @param mixed $phone
   */
  public function setPhone($phone) {
    $this->phone = $phone;
  }

  /**
   * @return mixed
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param mixed $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

}