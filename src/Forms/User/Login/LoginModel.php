<?php

namespace KoshTheme\Forms\User\Login;


use Symfony\Component\Validator\Constraints as Assert;

class LoginModel
{

    /**
     * @Assert\NotBlank(
     *     message="Обязательно поле"
     * )
     * @Assert\Email(
     *     message="Введите email адресс"
     * )
     */
    private $email;

    /**
     * @Assert\NotBlank(
     *     message="Обязательно поле"
     * )
     * @Assert\Regex(
     *   message="Минимум 4 символа, содержит 1 букву и цифру. Только английские символы.",
     *   pattern="/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,}$/"
     * )
     */
    private $password;

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

}