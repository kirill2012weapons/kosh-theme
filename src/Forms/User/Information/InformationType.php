<?php

namespace KoshTheme\Forms\User\Information;


use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\AbstractType;

class InformationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('logoUploadFile', FileType::class, [
            ])
            ->add('nickName', TextType::class, [
            ])
            ->add('name', TextType::class, [
            ])
            ->add('phone', TextType::class, [
            ])
            ->add('secondName', TextType::class, [
            ])
            ->add('description', TextType::class, [
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => InformationModel::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'information_form';
    }

}