<?php

namespace KoshTheme\Forms\User\Billing;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\AbstractType;

class BillingType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('billingFirstName', TextType::class, [
            ])
            ->add('billingSecondName', TextType::class, [
            ])
            ->add('billingCompany', TextType::class, [
            ])
            ->add('billingAddress1', TextType::class, [
            ])
            ->add('billingAddress2', TextType::class, [
            ])
            ->add('billingCity', TextType::class, [
            ])
            ->add('billingPostCode', TextType::class, [
            ])
            ->add('billingCountry', TextType::class, [
            ])
            ->add('billingState', TextType::class, [
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BillingModel::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'billing_form';
    }

}