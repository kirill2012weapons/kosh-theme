<?php if ( get_field('vgp_g_pr', 'opt_st_id') ) : ?>

  <?php
  /**
   * @var $wcProduct WC_Product
   * @var $product WP_Post
   */
  $product = get_field('vgp_g_pr', 'opt_st_id');
  $wcProduct = wc_get_product($product->ID);
  ?>

  <div class="ps-section--sale-off ps-section pt-80 pb-40">
  <div class="ps-container">
    <div class="ps-section__header mb-50">
      <h3 class="ps-section__title" data-mask="Акция">- Выгодное предложение</h3>
    </div>

    <div class="ps-section__content">
      <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 ">
          <div class="ps-hot-deal">
            <h3><?php echo $wcProduct->get_title() ?></h3>
            <p class="ps-hot-deal__price">Всего:  <span><?php echo $wcProduct->get_sale_price() ? $wcProduct->get_sale_price() : $wcProduct->get_regular_price() . ' ' . get_woocommerce_currency_symbol(); ?></span></p>
            <ul class="ps-countdown" data-time="<?php echo get_field('vgp_g_end_date', 'opt_st_id') ? get_field('vgp_g_end_date', 'opt_st_id') : 'December 13, 2019 15:37:25' ?>">
              <li><span class="hours"></span><p>Часы</p></li>
              <li class="divider">:</li>
              <li><span class="minutes"></span><p>Минуты</p></li>
              <li class="divider">:</li>
              <li><span class="seconds"></span><p>Секунды</p></li>

            </ul>

            <a class="ps-btn" href="<?php echo $wcProduct->get_permalink() ?>">
              Купить Сейчас<i class="ps-icon-next"></i>
            </a>

          </div>
        </div>

        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 ">
          <div class="ps-hotspot">

            <?php if (get_field('vgp_g_t_1', 'opt_st_id')) : ?>

              <a class="point first" href="javascript:;"><i class="fa fa-plus"></i>
                <div class="ps-hotspot__content">
                  <p class="heading"><?php echo get_field('vgp_g_t_1', 'opt_st_id'); ?></p>
                  <?php if (get_field('vgp_g_desc_1', 'opt_st_id')) : ?>
                    <p><?php echo get_field('vgp_g_desc_1', 'opt_st_id'); ?></p>
                  <?php endif; ?>
              </div>
              </a>

            <?php endif; ?>

            <?php if (get_field('vgp_g_t_2', 'opt_st_id')) : ?>

              <a class="point second" href="javascript:;"><i class="fa fa-plus"></i>
                <div class="ps-hotspot__content">
                  <p class="heading"><?php echo get_field('vgp_g_t_2', 'opt_st_id'); ?></p>
                  <?php if (get_field('vgp_g_desc_2', 'opt_st_id')) : ?>
                    <p><?php echo get_field('vgp_g_desc_2', 'opt_st_id'); ?></p>
                  <?php endif; ?>
                </div>
              </a>

            <?php endif; ?>

            <img src="<?php echo get_the_post_thumbnail_url( $wcProduct->get_id() ); ?>" alt="">

          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<?php endif; ?>