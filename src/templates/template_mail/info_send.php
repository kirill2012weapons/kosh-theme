<?php
/**
 * @var $contactModel \KoshTheme\Forms\Contacts\ContactModel
 */
?>

<table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer"
       style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border: 0; max-width: 600px !important;">
  <tbody>
  <tr>
    <td valign="top" id="templatePreheader"
        style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0; padding-top: 0px; padding-bottom: 0px;"></td>
  </tr>
  <tr>
    <td valign="top" id="templateHeader"
        style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0; padding-top: 0px; padding-bottom: 0;">
      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="CodeBlock"
             style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
        <tbody class="TextBlockOuter">
        <tr>
          <td valign="top" class="TextBlockInner"
              style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"></td>
        </tr>
        </tbody>
        <tbody>
        <tr>
          <td class="mobile-hide mobile-no-padding text-padding-left" width="450" height="110" align="left"
              style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding-left: 58px; mso-line-height-rule: exactly;">
            <a href="<?php echo $siteUrl; ?>" target="_blank"
               style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #111111; mso-line-height-rule: exactly; font-weight: normal; text-decoration: underline;"><img
                  src="<?php echo $siteUrlImg; ?>" alt="Themes Email" width="120" height="20"
                  style="border: 0; display: inline-block; font-size: 13px; font-weight: bold; height: auto; outline: none; text-decoration: none; text-transform: capitalize; vertical-align: middle; margin-right: 10px; -ms-interpolation-mode: bicubic;"
                  data-pagespeed-url-hash="2761749788" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"></a>
          </td>
        </tr>
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td valign="top" id="templateBody"
        style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 2px solid #EAEAEA; padding-top: 0; padding-bottom: 9px;">
      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="TextBlock"
             style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
        <tbody class="TextBlockOuter">
        <tr>
          <td valign="top" class="TextBlockInner" style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                   class="TextContentContainer">
              <tbody>
              <tr>
                <td valign="top" class="TextContent"
                    style="padding: 9px 18px; text-align: left; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #222222; font-family: Helvetica; font-size: 13px; line-height: 150%;">
                  <table border="0" cellpadding="0" cellspacing="0" max-width="564" width="100%"
                         style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
                    <tbody>
                    <tr>
                      <td id="emailcontent"
                          style="padding-left: 7%; padding-right: 7%; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">

                        <!--  Here Goes Content: Start  -->
                        <p style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #222222; font-family: Helvetica; font-size: 13px; line-height: 150%; text-align: left;">
                        Какой то хуя шо то заявляет
                        </p>


                        <div style="margin-bottom: 40px;">
                          <table class="td" cellspacing="0" cellpadding="6"
                                 style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4e4e4e; vertical-align: middle;">
                            <thead>
                            <tr>
                              <th class="td" scope="col"
                                  style="text-align: left; color: #4e4e4e; vertical-align: middle; border-bottom: 1px solid #ebebeb;">Поле
                              </th>
                              <th class="td" scope="col"
                                  style="text-align: left; color: #4e4e4e; vertical-align: middle; border-bottom: 1px solid #ebebeb;">Значение
                              </th>
                            </tr>
                            </thead>
                            <tbody>


                              <tr class="order_item">
                              <td class="td"
                                  style="text-align: left; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap: break-word; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4e4e4e; border-bottom: 1px solid #ebebeb;font-weight: bold;">
                                Имя
                              </td>
                              <td class="td"
                                  style="text-align: left; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4e4e4e; border-bottom: 1px solid #ebebeb;">
                                <?php echo $contactModel->getName() ?>
                              </td>
                              </tr>

                              <tr class="order_item">
                                <td class="td"
                                    style="text-align: left; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap: break-word; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4e4e4e; border-bottom: 1px solid #ebebeb;font-weight: bold;">
                                  Мыло
                                </td>
                                <td class="td"
                                    style="text-align: left; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4e4e4e; border-bottom: 1px solid #ebebeb;">
                                  <?php echo $contactModel->getEmail() ?>
                                </td>
                              </tr>

                              <tr class="order_item">
                                <td class="td"
                                    style="text-align: left; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap: break-word; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4e4e4e; border-bottom: 1px solid #ebebeb;font-weight: bold;">
                                  Телефон
                                </td>
                                <td class="td"
                                    style="text-align: left; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4e4e4e; border-bottom: 1px solid #ebebeb;">
                                  <?php echo $contactModel->getPhone() ?>
                                </td>
                              </tr>

                              <tr class="order_item">
                                <td class="td"
                                    style="text-align: left; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap: break-word; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4e4e4e; border-bottom: 1px solid #ebebeb;font-weight: bold;">
                                  Описание че хочет гнида
                                </td>
                                <td class="td"
                                    style="text-align: left; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4e4e4e; border-bottom: 1px solid #ebebeb;">
                                  <?php echo $contactModel->getDescription() ?>
                                </td>
                              </tr>

                            </tbody>
                          </table>
                        </div>

                      </td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td valign="top" id="templateFooter"
        style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ededed; border-top: 0; border-bottom: 0; padding-top: 9px; padding-bottom: 9px;">
      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="TextBlock"
             style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
        <tbody class="TextBlockOuter">
        <tr>
          <td valign="top" class="TextBlockInner" style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                   class="TextContentContainer">
              <tbody>
              <tr>
                <td valign="top" class="TextContent"
                    style="padding-top: 9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #656565; font-family: Helvetica; font-size: 12px; line-height: 150%; text-align: center;">
                  <div style="text-align: center;">
                    <br>
                    Дополнительная контактная информация по номеру - <?php echo $contactPhone; ?><br>
                    Email - <?php echo $contactEmail; ?>
                  </div>
                </td>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        </tbody>
      </table>
    </td>
  </tr>
  </tbody>
</table>