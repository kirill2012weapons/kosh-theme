<?php
/**
 * @var $checkoutFindOrderFormView \Symfony\Component\Form\FormView
 */
?>

<form class="ps-contact__form" action="?" method="post">
  <?php if ($checkoutFindOrderFormView->vars['errors']->count() > 0) : ?>
    <?php foreach ($checkoutFindOrderFormView->vars['errors'] as $error) : ?>
      <div style="margin-bottom: 3px;">
        <span style="color: #ff0013; font-size: 12px;">
            <?= $error->getMessage(); ?>
        </span>
      </div>
    <?php endforeach; ?>
  <?php endif; ?>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
      <div class="form-group">
        <label>Номер Заказа <sub>*</sub></label>

        <?php if ($checkoutFindOrderFormView->children['orderId']->vars['errors']->count() > 0) : ?>
          <?php foreach ($checkoutFindOrderFormView->children['orderId']->vars['errors'] as $error) : ?>
            <div style="margin-bottom: 3px;">
              <span style="color: #ff0013; font-size: 12px;">
                  <?= $error->getMessage(); ?>
              </span>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>

        <input class="form-control" type="text"
               name="<?= $checkoutFindOrderFormView->children['orderId']->vars['full_name'] ?>"
               value="<?= $checkoutFindOrderFormView->children['orderId']->vars['value'] ?>"
               placeholder="56326">
      </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
      <div class="form-group">
        <label>Хэш Заказа <sub>*</sub></label>

        <?php if ($checkoutFindOrderFormView->children['orderHash']->vars['errors']->count() > 0) : ?>
          <?php foreach ($checkoutFindOrderFormView->children['orderHash']->vars['errors'] as $error) : ?>
            <div style="margin-bottom: 3px;">
              <span style="color: #ff0013; font-size: 12px;">
                  <?= $error->getMessage(); ?>
              </span>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>

        <input class="form-control" type="text"
               name="<?= $checkoutFindOrderFormView->children['orderHash']->vars['full_name'] ?>"
               value="<?= $checkoutFindOrderFormView->children['orderHash']->vars['value'] ?>"
               placeholder="wc_order_jv0461DjcOOln">
      </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
      <div class="form-group">
        <button type="submit" class="ps-btn">Поиск<i class="ps-icon-next"></i></button>
      </div>
    </div>
  </div>
</form>
