<?php

$items = WC()->cart->get_cart();

?>

<!--<div>-->
<!--  <div class="row">-->
<!--    <div class="col-lg-6"></div>-->
<!--    <div class="col-lg-1"></div>-->
<!--    <div class="col-lg-3"></div>-->
<!--    <div class="col-lg-2"></div>-->
<!--  </div>-->
<!--  <div class="row">-->
<!--    <div class="col-lg-6"></div>-->
<!--    <div class="col-lg-1"></div>-->
<!--    <div class="col-lg-3"></div>-->
<!--    <div class="col-lg-2"></div>-->
<!--  </div>-->
<!--</div>-->

<table class="table ps-cart__table">
  <thead>
  <tr>
    <th>Товары</th>
    <th>Кол.</th>
    <th>Цена</th>
    <th></th>
  </tr>
  </thead>
  <tbody>
  <?php foreach ($items as $item => $values) : ?>
    <?php
    $product =  wc_get_product( $values['data']->get_id() );
    $getProductDetail = wc_get_product( $values['product_id'] );
    ?>
    <tr>
    <td>
      <a class="ps-product__preview" href="<?php echo get_post_permalink( $values['data']->get_id() ); ?>">
        <?php echo $getProductDetail->get_image('custom_size_100_100'); ?>
        <?php echo $product->get_title() ?>
      </a>
    </td>
    <td>
      <div class="form-group--number">
        <button data-remove-item-popup-1 class="minus"><span>-</span></button>
        <input data-value-card-popup data-it-cart-popup="<?php echo $values['data']->get_id(); ?>" class="form-control" type="text" value="<?php echo $values['quantity']; ?>" min="1">
        <button data-add-item-popup-1 class="plus"><span>+</span></button>
      </div>
    </td>
    <td><?= $product->get_price()*$values['quantity'] . ' ' . get_woocommerce_currency_symbol(); ?></td>
    <td>
      <div class="ps-remove"></div>
    </td>
  </tr>
  <?php endforeach; ?>
  </tbody>
</table>
<div class="ps-cart__actions">
  <div class="ps-cart__total">
    <h3>Стоимость Всего: <span> <?php echo WC()->cart->get_cart_total(); ?></span></h3><a class="ps-btn" href="<?php echo home_url('/checkout/') ?>">Купить<i class="ps-icon-next"></i></a>
  </div>
</div>
