<?php

use KoshTheme\Forms\User\Login\LoginModel;
use KoshTheme\Forms\User\Login\LoginType;


$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
/**
 * @var $formFactory \Symfony\Component\Form\FormFactory
 */
$formFactory = container()->get('factory.form.annotation_validator');

$loginModel = new LoginModel();
$loginForm = $formFactory->createBuilder(LoginType::class, $loginModel)->getForm();

$loginForm->handleRequest($request);

if ($loginForm->isSubmitted() && $loginForm->isValid()) {

  $user_data['user_login'] = $_POST['login_form']['email'];
  $user_data['user_password'] = $_POST['login_form']['password'];
  $user_data['remember'] = false;
  $user = wp_signon($user_data, false);

}

/**
 * @var $registerFormView \Symfony\Component\Form\FormView
 */
$loginFormView = $loginForm->createView();

?>

<form class="ps-contact__form" action="?" method="POST">
  <div class="row">
    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 ">
      <div class="form-group">
        <label>Логин <sub>*</sub></label>

        <?php if ($loginFormView->children['email']->vars['errors']->count() > 0) : ?>
          <?php foreach ($loginFormView->children['email']->vars['errors'] as $error) : ?>
            <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>

        <?php if (isset($user) && is_wp_error($user)) : ?>
          <div style="margin-bottom: 3px;">
            <span style="color: #ff0013; font-size: 12px;">
                <?php echo $user->get_error_message(); ?>
            </span>
          </div>
        <?php endif; ?>

        <input class="form-control"
               type="text"
               placeholder="example@gmail.com"
               name="<?= $loginFormView->children['email']->vars['full_name'] ?>"
               value="<?= $loginFormView->children['email']->vars['value'] ?>"
        >
      </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 ">
      <div class="form-group">
        <label>Пароль <sub>*</sub></label>

        <?php if ($loginFormView->children['password']->vars['errors']->count() > 0) : ?>
          <?php foreach ($loginFormView->children['password']->vars['errors'] as $error) : ?>
            <div style="margin-bottom: 3px;">
                            <span style="color: #ff0013; font-size: 12px;">
                                <?= $error->getMessage(); ?>
                            </span>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>

        <input class="form-control"
               type="password"
               placeholder=""
               name="<?= $loginFormView->children['password']->vars['full_name'] ?>"
        >
      </div>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 ">
      <div class="form-group">
        <button class="ps-btn">Ввойти<i class="ps-icon-next"></i></button>
      </div>
    </div>
  </div>
</form>


