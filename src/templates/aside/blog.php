<?php if (false) : ?>
  <aside class="ps-widget--sidebar ps-widget--search">
    <form class="ps-search--widget" action="do_action" method="post">
      <input class="form-control" type="text" placeholder="Search posts...">
      <button><i class="ps-icon-search"></i></button>
    </form>
  </aside>
  <aside class="ps-widget--sidebar">
    <div class="ps-widget__header">
      <h3>Archive</h3>
    </div>
    <div class="ps-widget__content">
      <ul class="ps-list--arrow">
        <li class="current"><a href="product-listing.html">Sky(321)</a></li>
        <li><a href="product-listing.html">Amazin’ Glazin’</a></li>
        <li><a href="product-listing.html">The Crusty Croissant</a></li>
        <li><a href="product-listing.html">The Rolling Pin</a></li>
        <li><a href="product-listing.html">Skippity Scones</a></li>
        <li><a href="product-listing.html">Mad Batter</a></li>
        <li><a href="product-listing.html">Confection Connection</a></li>
      </ul>
    </div>
  </aside>
  <aside class="ps-widget--sidebar">
    <div class="ps-widget__header">
      <h3>Ads Banner</h3>
    </div>
    <div class="ps-widget__content"><a href="product-listing"><img src="<?php HTML_IMG ?>/offer/sidebar.jpg" alt=""></a></div>
  </aside>
  <aside class="ps-widget--sidebar">
    <div class="ps-widget__header">
      <h3>Tags</h3>
    </div>
    <div class="ps-widget__content">
      <ul class="ps-tags">
        <li><a href="product-listing.html">Men</a></li>
        <li><a href="product-listing.html">Female</a></li>
        <li><a href="product-listing.html">B&amp;G</a></li>
        <li><a href="product-listing.html">ugly fashion</a></li>
        <li><a href="product-listing.html">Nike</a></li>
        <li><a href="product-listing.html">Dior</a></li>
        <li><a href="product-listing.html">Adidas</a></li>
        <li><a href="product-listing.html">Diour</a></li>
      </ul>
    </div>
  </aside>
<?php endif; ?>


<?php
$lastPostsArgs = [
  'post_type' => 'blog_pt',
  'post_status' => 'publish',
  'posts_per_page' => 5,
];
$lastPosts = new WP_Query($lastPostsArgs);
?>
<?php if (!empty($lastPosts->posts)) : ?>

  <aside class="ps-widget--sidebar">
    <div class="ps-widget__header">
      <h3>Новости</h3>
    </div>
    <div class="ps-widget__content">
      <?php foreach ($lastPosts->posts as $post) : ?>
        <?php
        /**
         * @var $post WP_Post
         */
        $id = $post->ID;
        ?>
        <div class="ps-post--sidebar">
          <div class="ps-post__thumbnail"><a href="#"></a>
            <img src="<?php echo get_the_post_thumbnail_url($id, 'post-thumbnail') ? get_the_post_thumbnail_url($id, 'post-thumbnail') : PHP_IMG . '/placeholder.png'; ?>" alt="">
          </div>
          <div class="ps-post__content">
            <a class="ps-post__title" href="<?php echo get_post_permalink($id); ?>"><?php echo get_the_title($id); ?></a>
            <span><?php echo get_the_date('j F Y', $id); ?></span>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </aside>

<?php endif; ?>


<?php
$bestSales = get_field('tops_g_tov', 'opt_st_id');
?>
<?php if (!empty($bestSales)) : ?>
  <aside class="ps-widget--sidebar">
    <div class="ps-widget__header">
      <h3>Лучшие продажи:</h3>
    </div>
    <div class="ps-widget__content">
      <?php foreach ($bestSales as $product) : ?>
        <?php
        /**
         * @var $product WP_Post
         * @var $wcProduct WC_Product
         */
        $wcProduct = wc_get_product($product->ID)
        ?>
        <div class="ps-shoe--sidebar">
          <div class="ps-shoe__thumbnail">
            <a href="<?php echo get_post_permalink($wcProduct->get_id()); ?>"></a>
            <img src="<?php echo get_the_post_thumbnail_url($wcProduct->get_id(), 'post-thumbnail') ?>" alt="">
          </div>
          <div class="ps-shoe__content">
            <a class="ps-shoe__title" href="<?php echo get_post_permalink($wcProduct->get_id()); ?>"><?php echo $wcProduct->get_title(); ?></a>

            <?php
            /**
             * SALE PRISE AND %
             */
            $price = get_post_meta($wcProduct->get_id(), '_regular_price', true);
            $price_sale = get_post_meta($wcProduct->get_id(), '_sale_price', true);
            ?>

            <p style="margin-bottom: 6px;">
              <?php if ($price_sale !== '') : ?>
                <del><?= $price . ' ' . get_woocommerce_currency_symbol(); ?></del> <?= $price_sale . ' ' . get_woocommerce_currency_symbol(); ?>
              <?php else: ?>
                <del></del><?= $price . ' ' . get_woocommerce_currency_symbol(); ?>
              <?php endif; ?>
            </p>

            <a style="margin-top: 0; margin-bottom: 15px;" class="ps-btn" href="<?php echo get_post_permalink($wcProduct->get_id()); ?>">Купить</a>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </aside>
<?php endif; ?>