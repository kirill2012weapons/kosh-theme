<?php
/**
 * @var $curUser WP_User
 */
?>
<div class="ps-author">
  <div class="ps-author__thumbnail"><img src="<?php echo get_avatar_url($curUser->ID); ?>" alt="Avatar"></div>
  <div class="ps-author__content" style="width: 100%;">

    <?php if (isset($currUserMeta->nickname) && $currUserMeta->nickname[0]) : ?>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-4 col-md-12 col-sm-12" style="vertical-align: center;">
          <p style="margin: 0;">Nick Name</p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12" style="padding-left: 5px; border-left: 2px solid #2AC37D; vertical-align: center;">
          <h4 style="margin: 0;font-size: 16px;"><?php echo $currUserMeta->nickname[0]; ?></h4>
        </div>
      </div>
    <?php endif; ?>

    <?php if (isset($currUserMeta->first_name) && $currUserMeta->first_name[0]) : ?>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-4 col-md-12 col-sm-12" style="vertical-align: center;">
          <p style="margin: 0;">Имя</p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12" style="padding-left: 5px; border-left: 2px solid #2AC37D; vertical-align: center;">
          <h4 style="margin: 0;font-size: 16px;"><?php echo $currUserMeta->first_name[0]; ?></h4>
        </div>
      </div>
    <?php endif; ?>

    <?php if (isset($currUserMeta->last_name) && $currUserMeta->last_name[0]) : ?>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-4 col-md-12 col-sm-12" style="vertical-align: center;">
          <p style="margin: 0;">Фамилия</p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12" style="padding-left: 5px; border-left: 2px solid #2AC37D; vertical-align: center;">
          <h4 style="margin: 0;font-size: 16px;"><?php echo $currUserMeta->last_name[0]; ?></h4>
        </div>
      </div>
    <?php endif; ?>

    <?php if (isset($currUserMeta->billing_) && $currUserMeta->billing_[0]) : ?>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-4 col-md-12 col-sm-12" style="vertical-align: center;">
          <p style="margin: 0;">Телефон</p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12" style="padding-left: 5px; border-left: 2px solid #2AC37D; vertical-align: center;">
          <h4 style="margin: 0;font-size: 16px;"><?php echo $currUserMeta->billing_[0]; ?></h4>
        </div>
      </div>
    <?php endif; ?>

    <?php if (isset($currUserMeta->description) && $currUserMeta->description[0]) : ?>
      <div class="ps-post--detail">
        <div class="ps-post__content">
          <blockquote>
            <p>
              <?php echo nl2br($currUserMeta->description[0]); ?>
            </p>
          </blockquote>
        </div>
      </div>
    <?php endif; ?>

    <h3 style="margin-bottom: 15px; margin-top: 15px; padding-bottom: 15px;font-size: 16px; border-bottom: 2px solid #2AC37D;">Платежные данные</h3>

    <?php if (isset($currUserMeta->billing_first_name) && $currUserMeta->billing_first_name[0]) : ?>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-4 col-md-12 col-sm-12" style="vertical-align: center;">
          <p style="margin: 0;">Платежное имя</p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12" style="padding-left: 5px; border-left: 2px solid #2AC37D; vertical-align: center;">
          <h4 style="margin: 0;font-size: 16px;"><?php echo $currUserMeta->billing_first_name[0]; ?></h4>
        </div>
      </div>
    <?php endif; ?>

    <?php if (isset($currUserMeta->billing_last_name) && $currUserMeta->billing_last_name[0]) : ?>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-4 col-md-12 col-sm-12" style="vertical-align: center;">
          <p style="margin: 0;">Платежная фамилия</p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12" style="padding-left: 5px; border-left: 2px solid #2AC37D; vertical-align: center;">
          <h4 style="margin: 0;font-size: 16px;"><?php echo $currUserMeta->billing_last_name[0]; ?></h4>
        </div>
      </div>
    <?php endif; ?>

    <?php if (isset($currUserMeta->billing_company) && $currUserMeta->billing_company[0]) : ?>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-4 col-md-12 col-sm-12" style="vertical-align: center;">
          <p style="margin: 0;">Платежная компания</p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12" style="padding-left: 5px; border-left: 2px solid #2AC37D; vertical-align: center;">
          <h4 style="margin: 0;font-size: 16px;"><?php echo $currUserMeta->billing_company[0]; ?></h4>
        </div>
      </div>
    <?php endif; ?>

    <?php if (isset($currUserMeta->billing_address_1) && $currUserMeta->billing_address_1[0]) : ?>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-4 col-md-12 col-sm-12" style="vertical-align: center;">
          <p style="margin: 0;">Платежный аддресс</p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12" style="padding-left: 5px; border-left: 2px solid #2AC37D; vertical-align: center;">
          <h4 style="margin: 0;font-size: 16px;"><?php echo $currUserMeta->billing_address_1[0]; ?></h4>
        </div>
      </div>
    <?php endif; ?>

    <?php if (isset($currUserMeta->billing_address_2) && $currUserMeta->billing_address_2[0]) : ?>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-4 col-md-12 col-sm-12" style="vertical-align: center;">
          <p style="margin: 0;">Платежный аддресс 2</p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12" style="padding-left: 5px; border-left: 2px solid #2AC37D; vertical-align: center;">
          <h4 style="margin: 0;font-size: 16px;"><?php echo $currUserMeta->billing_address_2[0]; ?></h4>
        </div>
      </div>
    <?php endif; ?>

    <?php if (isset($currUserMeta->billing_city) && $currUserMeta->billing_city[0]) : ?>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-4 col-md-12 col-sm-12" style="vertical-align: center;">
          <p style="margin: 0;">Платежный город</p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12" style="padding-left: 5px; border-left: 2px solid #2AC37D; vertical-align: center;">
          <h4 style="margin: 0;font-size: 16px;"><?php echo $currUserMeta->billing_city[0]; ?></h4>
        </div>
      </div>
    <?php endif; ?>

    <?php if (isset($currUserMeta->billing_postcode) && $currUserMeta->billing_postcode[0]) : ?>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-4 col-md-12 col-sm-12" style="vertical-align: center;">
          <p style="margin: 0;">Платежный индекс</p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12" style="padding-left: 5px; border-left: 2px solid #2AC37D; vertical-align: center;">
          <h4 style="margin: 0;font-size: 16px;"><?php echo $currUserMeta->billing_postcode[0]; ?></h4>
        </div>
      </div>
    <?php endif; ?>

    <?php if (isset($currUserMeta->billing_country) && $currUserMeta->billing_country[0]) : ?>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-4 col-md-12 col-sm-12" style="vertical-align: center;">
          <p style="margin: 0;">Платежная страна</p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12" style="padding-left: 5px; border-left: 2px solid #2AC37D; vertical-align: center;">
          <h4 style="margin: 0;font-size: 16px;"><?php echo $currUserMeta->billing_country[0]; ?></h4>
        </div>
      </div>
    <?php endif; ?>

    <?php if (isset($currUserMeta->billing_state) && $currUserMeta->billing_state[0]) : ?>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-4 col-md-12 col-sm-12" style="vertical-align: center;">
          <p style="margin: 0;">Платежное государство</p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12" style="padding-left: 5px; border-left: 2px solid #2AC37D; vertical-align: center;">
          <h4 style="margin: 0;font-size: 16px;"><?php echo $currUserMeta->billing_state[0]; ?></h4>
        </div>
      </div>
    <?php endif; ?>

    <?php if (isset($currUserMeta->billing_phone) && $currUserMeta->billing_phone[0]) : ?>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-4 col-md-12 col-sm-12" style="vertical-align: center;">
          <p style="margin: 0;">Телефонный номер</p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12" style="padding-left: 5px; border-left: 2px solid #2AC37D; vertical-align: center;">
          <h4 style="margin: 0;font-size: 16px;"><?php echo $currUserMeta->billing_phone[0]; ?></h4>
        </div>
      </div>
    <?php endif; ?>

    <?php if (isset($currUserMeta->billing_email) && $currUserMeta->billing_email[0]) : ?>
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-4 col-md-12 col-sm-12" style="vertical-align: center;">
          <p style="margin: 0;">Платежная Почта</p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12" style="padding-left: 5px; border-left: 2px solid #2AC37D; vertical-align: center;">
          <h4 style="margin: 0;font-size: 16px;"><?php echo $currUserMeta->billing_email[0]; ?></h4>
        </div>
      </div>
    <?php endif; ?>

  </div>
</div>