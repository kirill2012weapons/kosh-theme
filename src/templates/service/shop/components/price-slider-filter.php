<?php
/**
 * @var $priceMin \Symfony\Component\Form\FormView
 * @var $priceMax \Symfony\Component\Form\FormView
 */
?>
<aside class="ps-widget--sidebar ps-widget--filter">
  <div class="ps-widget__header">
    <h3>Цена</h3>
  </div>
  <input type="hidden"
         name="<?php echo $priceMin->vars['full_name'] ?>"
         value="<?php echo $priceMin->vars['value'] ?>"
  >
  <input type="hidden"
         name="<?php echo $priceMax->vars['full_name'] ?>"
         value="<?php echo $priceMax->vars['value'] ?>"
  >

  <div class="ps-widget__content">
    <div class="ac-slider" data-default-min="<?php echo $priceMin->vars['value'] ?>" data-default-max="<?php echo $priceMax->vars['value'] ?>" data-max="2000" data-step="50" data-unit="₴">
    </div>
    <p class="ac-slider__meta">Цена:<span class="ac-slider__value ac-slider__min">
      </span>-<span class="ac-slider__value ac-slider__max"></span>
    </p>

    <input value="Применить" type="submit" class="ac-slider__filter ps-btn">
  </div>
</aside>
