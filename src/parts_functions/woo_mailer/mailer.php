<?php

function sendMailCustomOrder(WC_Order $order) {

  $recipient = $order->get_billing_email();
  $subject = "Информация о заказе" . ' - ' .  get_bloginfo('name');

  $content = render('template_mail/order_send.php', [
    'siteUrl' => site_url(),
    'siteUrlImg' => HTML_IMG . '/logo.png',
    'order' => $order,
    'contactPhone' => '+380999013672',
    'contactEmail' => get_option('admin_email')
  ]);

  $swpsmtpOptions = get_option( 'swpsmtp_options' );

  $headers = "Content-Type: text/html\r\n";
  $headers .= "From: " . $swpsmtpOptions['from_name_field'] . " <" . $swpsmtpOptions['from_email_field'] . ">\r\n";

  $result = wp_mail( $recipient, $subject, $content, $headers );

  logCustom('----------------------------- ' . $result);

  if (!$result) logCustom('Не отправилось на почту пистьмо. Номер заказа - ' . $order->get_id());

}

function sendMailCustomInfo(\KoshTheme\Forms\Contacts\ContactModel $contactModel) {

  $subject = "Информация Какая то гнида" . ' - ' .  get_bloginfo('name');

  $content = render('template_mail/info_send.php', [
    'siteUrl' => site_url(),
    'siteUrlImg' => HTML_IMG . '/logo.png',
    'contactPhone' => '+380999013672',
    'contactEmail' => get_option('admin_email'),
    'contactModel' => $contactModel,
  ]);

  /**
   * $swpsmtpOptions['from_email_field']
   */

  $swpsmtpOptions = get_option( 'swpsmtp_options' );

  $headers = "Content-Type: text/html\r\n";
  $headers .= "From: " . $swpsmtpOptions['from_name_field'] . " <" . $swpsmtpOptions['from_email_field'] . ">\r\n";

  $result = wp_mail( $swpsmtpOptions['from_email_field'], $subject, $content, $headers );

  if (!$result) logCustom('Не отправилось на почту пистьмо. Обычный контакт сучка');

  return $result;

}