<?php
/**
 * Template Name: Account Page
 */
?>

<?php
if (!is_user_logged_in()) {
  wp_safe_redirect(home_url());
  die();
}
?>

<?php
$curUser = wp_get_current_user();
$currUserMeta = (object) get_user_meta($curUser->ID);
?>

<?php get_header(); ?>

<div class="ps-product--detail pt-60">
  <div class="ps-container">
    <div class="row">
      <div class="col-lg-10 col-md-12 col-lg-offset-1">
        <div class="ps-product__content mt-50">
          <ul class="tab-list" role="tablist">
            <li class="active"><a href="#account_info" aria-controls="account_info" role="tab" data-toggle="tab">Информация</a>
            </li>
            <li><a href="#account_info_edit" aria-controls="account_info_edit" role="tab" data-toggle="tab">Редактировать - <?php echo $curUser->nickname ?></a></li>
            <li><a href="#account_info_history" aria-controls="account_info_history" role="tab" data-toggle="tab">История</a></li>
            <li><a href="#account_info_wishes" aria-controls="account_info_wishes" role="tab" data-toggle="tab">Желания</a></li>
          </ul>
        </div>
        <div class="tab-content mb-60">

          <div class="tab-pane active" role="tabpanel" id="account_info">
            <?php echo render('users-tab_info/user-information.php', [
              'curUser'       => $curUser,
              'currUserMeta'  => $currUserMeta,
            ]); ?>
          </div>

          <div class="tab-pane" role="tabpanel" id="account_info_edit">
            <?php echo render('users-tab_info/user-information-edit.php', [
              'curUser'       => $curUser,
              'currUserMeta'  => $currUserMeta,
            ]); ?>
          </div>

          <div class="tab-pane" role="tabpanel" id="account_info_history">
            <?php echo render('users-tab_info/user-information-history.php', [
              'curUser'       => $curUser,
              'currUserMeta'  => $currUserMeta,
            ]); ?>
          </div>


          <div class="tab-pane" role="tabpanel" id="account_info_wishes">
            <?php echo render('users-tab_info/user-information-wishes.php', [
              'curUser'       => $curUser,
              'currUserMeta'  => $currUserMeta,
            ]); ?>
          </div>


        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
