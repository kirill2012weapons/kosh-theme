
          <?php if (false) : ?>
            <div class="ps-subscribe">
                <div class="ps-container">
                    <div class="row">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 ">
                            <h3><i class="fa fa-envelope"></i>Sign up to Newsletter</h3>
                        </div>
                        <div class="col-lg-5 col-md-7 col-sm-12 col-xs-12 ">
                            <form class="ps-subscribe__form" action="do_action" method="post">
                                <input class="form-control" type="text" placeholder="">
                                <button>Sign up now</button>
                            </form>
                        </div>
                        <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12 ">
                            <p>...and receive  <span>$20</span>  coupon for first shopping.</p>
                        </div>
                    </div>
                </div>
            </div>
          <?php endif; ?>


            <div class="ps-footer bg--cover" data-background="<?= PHP_IMG; ?>/fon.jpg">
                <div class="ps-footer__content">
                    <div class="ps-container">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                                <aside class="ps-widget--footer ps-widget--info">
                                    <header><a class="ps-logo" href="index.html"><img src="<?= HTML_IMG; ?>/logo-white.png" alt=""></a>
                                        <h3 class="ps-widget__title">Адресс 1</h3>
                                    </header>
                                    <footer>
                                        <p><strong>460 West 34th Street, 15th floor, New York</strong></p>
                                        <p>Email: <a href='mailto:support@store.com'>support@store.com</a></p>
                                        <p>Phone: +323 32434 5334</p>
                                        <p>Fax: ++323 32434 5333</p>
                                    </footer>
                                </aside>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                                <aside class="ps-widget--footer ps-widget--info second">
                                    <header>
                                        <h3 class="ps-widget__title">Адресс 2</h3>
                                    </header>
                                    <footer>
                                        <p><strong>PO Box 16122 Collins  Victoria 3000 Australia</strong></p>
                                        <p>Email: <a href='mailto:support@store.com'>support@store.com</a></p>
                                        <p>Phone: +323 32434 5334</p>
                                        <p>Fax: ++323 32434 5333</p>
                                    </footer>
                                </aside>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                <aside class="ps-widget--footer ps-widget--link">
                                    <header>
                                        <h3 class="ps-widget__title">ССЫЛКИ</h3>
                                    </header>
                                    <footer>
                                        <ul class="ps-list--link">
                                            <li><a href="<?= home_url(); ?>">ГЛАВНАЯ</a></li>
                                            <li><a href="<?php echo get_post_type_archive_link('blog_pt') ?>">Новости</a></li>
                                            <li><a href="<?php echo home_url('/contact/') ?>">Контакты</a></li>
                                            <li><a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>">Магазин</a></li>
                                        </ul>
                                    </footer>
                                </aside>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                <aside class="ps-widget--footer ps-widget--link">
                                    <header>
                                        <h3 class="ps-widget__title">ПРОФИЛЬ</h3>
                                    </header>
                                    <footer>
                                        <ul class="ps-list--line">
                                          <?php if (is_user_logged_in()) : ?>
                                            <?php
                                            /**
                                             * @var $currentUser WP_User
                                             */
                                            $currentUser = wp_get_current_user();
                                            ?>
                                            <li><a href="<?php echo home_url('/account/'); ?>"><?= $currentUser->display_name; ?></a></li>
                                          <?php else: ?>
                                            <li><a href="<?= home_url('/register/') ?>">Регистрация / Войти</a></li>
                                          <?php endif; ?>

                                        </ul>
                                    </footer>
                                </aside>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                                <aside class="ps-widget--footer ps-widget--link">
                                    <header>
                                        <h3 class="ps-widget__title">ЗАКАЗЫ</h3>
                                    </header>
                                    <footer>
                                        <ul class="ps-list--line">
                                            <li><a href="<?php echo home_url('/order-finder/') ?>">СТАТУС ЗАКАЗА</a></li>
                                            <li><a href="#cart-popup">КОРЗИНА</a></li>
                                        </ul>
                                    </footer>
                                </aside>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ps-footer__copyright">
                    <div class="ps-container">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                <p>&copy; <a href="<?php echo home_url(); ?>">DARKSIDE</a>, Сделал Кирюша - по вопросам - kirill2012weapons@gmail.com</p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                <ul class="ps-social">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </main>

        <?php wp_footer(); ?>

        <?php echo render('popup/cart.php'); ?>

        <script>
          document.addEventListener("DOMContentLoaded", function(event) {
            <?php
            $notices = wc_get_notices('success');
              foreach ($notices as $noteState => $note) {
                echo 'bootbox.alert("' . $note . '");';
              }
              wc_clear_notices();
            ?>
          });
        </script>

    </body>

</html>